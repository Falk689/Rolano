// needed for compass and I2C connections
#include <Wire.h>
#include <DFRobot_QMC5883.h>
#include <ADXL345.h>

// directions
#define S            0                                                  // don't move
#define F            1                                                  // move forward
#define B            2                                                  // move backward
#define L            3                                                  // turn left
#define R            4                                                  // turn right

// bluetooth pins
#define BT_TX_PIN    18
#define BT_RX_PIN    19

// wheel switches pins
#define LX_SWITCH    52
#define RX_SWITCH    53

// loop consts
#define FIRDELAY     40                                                 // loops to wait before checking lower IR sensors (fall sensors)
#define LIRDELAY     16                                                 // loops to wait between single sensors check (fall sensors)
#define CIRDELAY     150                                                // loops to wait before checking the remote control
#define NAIDELAY     80                                                 // 
#define AIBDELAY     400                                                // loops to wait before checking the AI under normal conditions (AI Base delay)
#define AITDELAY     5                                                  // loops to wait before checking the AI and the compass while turning 

// modes consts
#define OPMODE       3                                                  // used to distinguish between normal operation and emergency modes,
                                                                        // we don't accept state change controls during emergencies
                                                                        // basically this is the index for manual control mode

#define EMGDELAY     200                                                // basic emergency delay

// IR consts 
#define NORMAL_READS 4                                                  // how many times we read a IR sensor normally
#define START_READS  10                                                 // how many times we read a IR sensor on start to make an average

#define IRS          4                                                  // how many IR sensors we have

const byte IRpins[IRS]      = {A0, A1, A2, A3};                         // IR receiver analog pins
const byte IRemitters[IRS]  = {2, 3, 4, 5};                             // IR emitter LED digital pins

// motor consts
const byte MEnable[2]       = {8, 11};                                  // L293D EN pins (enable or disable motors)
const byte DXMotor[2]       = {9, 10};                                  // pins for the right motor transistors
const byte SXMotor[2]       = {12, 13};                                 // pins for the left motor transistors

// loop vars
int FallSensorsDelay        = FIRDELAY;                                 // Low Sensors delay variable so we actually check every x loops
int AIDelay                 = AIBDELAY;                                 // Artificial Intelligence delay, this way we can inform the AI if shit happens dinamically
int lCount;                                                             // loop counter

// IR vars
byte threshold[IRS]         = {200, 200, 200, 200};                     // under this threshold we trigger an alarm
byte lowthreshold           = 180;                                      // if we get more than this number of alarms in a row we lower average
byte decSens                = 5;                                        // after how many alarm-less checks we start decreasing alarms count

bool readFallSensors;                                                   // sensors state, prevents the usage of delay() to trigger sensors
bool fallAlarm[IRS];                                                    // store fall alarms to react properly
int  average[IRS];                                                      // average value of the sensor on normal distance
int  tmpAverage[IRS];                                                   // we'll confront these values with average[] to confirm we've got a problem
int  ambientIR[IRS];                                                    // stored ambient IR values
byte alarms[IRS];                                                       // how many alarms we had
byte lastAlarm[IRS];                                                    // count after an alarm so we don't suddenly decrease alarms count 
byte fallAlarmCount;                                                    // are we about to fall?
byte readingSensor;                                                     // what fall sensor we're reading
byte sensorsReads;                                                      // how many times we read all fall sensors


// motor vars
byte move                 = S;                                          // where we're moving

// IA vars
byte patrol[8]            = {L, S, R, S, R, S, L, S};                   // testing patrol mode (move states)
int  pDelay[8]            = {90, 1000, 90, 1000, 90, 1000, 90, 1000};   // for how many milliseconds our patrol states should last

byte stairLeft[4]         = {B, R, F, L};                               // what to do when we're about fall from the left side (move states)
byte stairRight[4]        = {B, L, F, R};                               // what to do when we're about fall from the right side (move states)
int  stairDelay[4]        = {200, 25, 800, 30};                         // for how many milliseconds our patrol states should last

byte controlMode[2];                                                    // control mode, emergency modes: (0=emergency, 1=stairleft, 2=stairright)
                                                                        // normal operation modes: (3=manual, 4=patrol)
                                                                        // state 0 does nothing on its own

byte AIState;                                                           // we store our current state here so we know what to do next

// bluetooth vars
char btData;                                                            // bluetooth data

// compass vars
DFRobot_QMC5883 compass;                                                // our compass
float heading;                                                          // where we're heading
float sHeading;                                                         // we were heading here when we started turning
float tHeading;                                                         // where we should head

float declinationAngle    = (2.0 + (17.0 / 60.0)) / (180 / PI);         // used to increase compass precision based on location

// accelerometer vars
ADXL345 accelerometer;



void setup()
{
   byte x, y;

   // initializing IR fall sensors emitters
   for (x=0; x<IRS; x++)
   {
      pinMode(IRemitters[x], OUTPUT);
      digitalWrite(IRemitters[x], LOW);
   }

   // initializing motor controls
   for (x=0; x<2; x++)
   {
      pinMode(SXMotor[x], OUTPUT);
      digitalWrite(SXMotor[x], LOW);

      pinMode(DXMotor[x], OUTPUT);
      digitalWrite(DXMotor[x], LOW);

      pinMode(MEnable[x], OUTPUT);
      digitalWrite(MEnable[x], LOW);
   }

   // initializing bluetooth pins
   pinMode(BT_RX_PIN, INPUT);
   pinMode(BT_TX_PIN, OUTPUT);

   // initializing lift switches
   pinMode(LX_SWITCH, INPUT);
   pinMode(RX_SWITCH, INPUT);

   // setting manual control (testing)
   controlMode[0] = OPMODE;

   // initializing wire library
   Wire.begin();

   // initializing serial connections
   Serial1.begin(9600); // bluetooth serial connection
   Serial.begin(9600);  // USB serial connection

   // initializing compass
   while (!compass.begin())
   {
      Serial.println("Could not find a valid QMC5883 sensor, check wiring!");
      delay(500);
   }

   if(compass.isHMC())
   {
      Serial.println("Initialize HMC5883");
      compass.setRange(HMC5883L_RANGE_1_3GA);
      compass.setMeasurementMode(HMC5883L_CONTINOUS);
      compass.setDataRate(HMC5883L_DATARATE_15HZ);
      compass.setSamples(HMC5883L_SAMPLES_8);
   }

   else if(compass.isQMC())
   {
      Serial.println("Initialize QMC5883");
      compass.setRange(QMC5883_RANGE_2GA);
      compass.setMeasurementMode(QMC5883_CONTINOUS); 
      compass.setDataRate(QMC5883_DATARATE_50HZ);
      compass.setSamples(QMC5883_SAMPLES_8);
   }

   // initializing accelerometer
   while (!accelerometer.begin())
   {
      Serial.println("Could not find a valid ADXL345 sensor, check wiring!");
      delay(500);
   }

   accelerometer.setRange(ADXL345_RANGE_2G);

   // initializing tHeading to its default value
   tHeading = -1;
}


void loop()
{
   byte x;
   bool lxState, rxState;

   // fall sensors
   if (lCount == FallSensorsDelay)
   {
      // first check the motor switches
      lxState = digitalRead(LX_SWITCH);
      rxState = digitalRead(RX_SWITCH);

      // the robot was probably lifted or we're hanging into the void
      // struggling won't help at this point, wait for help
      if (lxState == HIGH || rxState == HIGH)
      {
         // setting all sensors to alarm state
         for (x=0; x<IRS; x++)
            fallAlarm[x] = true;

         fall_emergency(lCount);

         FallSensorsDelay = AIBDELAY;
      }

      // the switches are open, check the sensors
      else
         read_lower_ir();

      // reset the delay here only if needed 
      if (FallSensorsDelay == lCount)
      {
         sensorsReads++;

         if (sensorsReads == 0)
         {

            FallSensorsDelay += FIRDELAY;

            if (fallAlarmCount > 0)
               fall_emergency(lCount);
         }

         else
         {
            FallSensorsDelay += LIRDELAY;
            fallAlarmCount = 0;
         }
      }

   }

   // AI loop
   if (lCount == AIDelay)
      ai_step(lCount);


   // Bluetooth control
   if (lCount == CIRDELAY)
   {
      //Serial.println(Serial1.available());

      bluetooth_control(false);

      // resetting variables

      // fall sensors
      if (FallSensorsDelay > CIRDELAY)
         FallSensorsDelay = FallSensorsDelay - CIRDELAY;

      else
         FallSensorsDelay = FIRDELAY;

      // AI
      if (AIDelay > CIRDELAY)
         AIDelay -= CIRDELAY;

      else if (AIDelay > 0)
         AIDelay = AIBDELAY;

      lCount = 0;
   }

   else
      lCount++;

   delay(1);
}


// "async" check on fall sensors
void read_lower_ir()
{
   int  distance;

   if (readFallSensors)
   {
      distance = readIR(readingSensor, true);

      // we're done calculating the average, return if we're about to fall
      if (sensorsReads == IRS)
      {
         //Serial.println("READ");
         // use stored temp average
         distance = (distance + tmpAverage[readingSensor]) / NORMAL_READS;
         tmpAverage[readingSensor] = 0;

         fallAlarm[readingSensor] = distance < average[readingSensor] - threshold[readingSensor];

         // we got an alarm increase alarms count
         if (fallAlarm[readingSensor])
         {
            fallAlarmCount++;
            alarms[readingSensor] = min(lowthreshold, alarms[readingSensor]++);
         }

         // no alarm, decreasing the average value solved the issue, reset the counter
         else if (alarms[readingSensor] >= lowthreshold)
            alarms[readingSensor] = 0;

         // no alarm, start counting and then slowly decrease the counter 
         else
         {
            // we reached alarms[readingSensor] 0, reset lastAlarm.
            if (alarms[readingSensor] <= 0)
               lastAlarm[readingSensor] = 0;

            // we're ok to decrease
            else if (lastAlarm[readingSensor] < decSens)
               alarms[readingSensor] = max(0, alarms[readingSensor]--);

            // count 'till we reach decSens
            else
               lastAlarm[readingSensor]++;

         }

         // tweak the average value depending on external conditions
         if (!fallAlarm[readingSensor])
            average[readingSensor] = (average[readingSensor] * 3 + distance) / 4;

         // tweak the average value to fix alarms on bad sensors (dangerous)
         else if (alarms[readingSensor] >= lowthreshold)
            average[readingSensor] = (average[readingSensor]  + distance) / 2;

         /*if (readingSensor == 1)
           {
           Serial.print(" pin: ");
           Serial.print(readingSensor);
           Serial.print(" value: ");
           Serial.print(distance);
           Serial.print(" average: ");
           Serial.print(average[readingSensor]);
           Serial.print(" threshold: ");
           Serial.print(average[readingSensor] - threshold[readingSensor]);
           Serial.print(" alarms: ");
           Serial.print(alarms[readingSensor]);
           Serial.print(" alarm: ");
           Serial.println(fallAlarm[readingSensor]);
           }*/

         // setting readSensor to false and changing check sensor
         readFallSensors = false;

         if (readingSensor < IRS - 1)
         {
            readingSensor++;
            FallSensorsDelay += 1;
         }

         else
         {
            readingSensor = 0;
            sensorsReads  = -1;
         }

         return;
      }

      // store temp average and return false
      else
      {
         //Serial.print("TMPREAD ");
         //Serial.println(readingSensor);

         if (tmpAverage[readingSensor] == 0)
            tmpAverage[readingSensor] = distance;

         else
            tmpAverage[readingSensor] = (tmpAverage[readingSensor] + distance) / 2;

         if (readingSensor < IRS - 1)
         {
            readingSensor++;
         }

         else
         {
            readingSensor = 0;
            readFallSensors = false;
            return;
         }

         readFallSensors = false;
      }
   }

   if (!readFallSensors)
   {
      //Serial.print("WRITE ");
      //Serial.println(readingSensor);
      FallSensorsDelay += 1;
      readIR(readingSensor, false);
      readFallSensors = true;
   }
}

// change move state and directly trigger the motors functions
void move_state(byte state)
{
   byte x;

   if (move != state)
   {
      //Serial.print("Changing state: ");

      move = state; // this is the only allowed place to edit move

      if (state != S)
      {
         // unlock the motors only if needed
         for (x=0; x<2; x++)
            digitalWrite(MEnable[x], HIGH);

         // move forward
         if (state == F)
            move_forward();

         // move backward
         else if (state == B)
            move_backward();

         // turn left
         else if (state == L)
            turn_left();

         // turn right
         else if (state == R)
            turn_right();
      }

      // stop
      else 
         stop();
   }
}

// all ahead full, aye!
void move_forward()
{
   digitalWrite(DXMotor[1], LOW);
   digitalWrite(SXMotor[1], LOW);

   digitalWrite(DXMotor[0], HIGH);
   digitalWrite(SXMotor[0], HIGH);
}

// moonwalk
void move_backward()
{
   digitalWrite(DXMotor[0], LOW);
   digitalWrite(SXMotor[0], LOW);

   digitalWrite(DXMotor[1], HIGH);
   digitalWrite(SXMotor[1], HIGH);
}

// turn left
void turn_left()
{
   digitalWrite(DXMotor[1], LOW);
   digitalWrite(SXMotor[0], LOW);

   digitalWrite(DXMotor[0], HIGH);
   digitalWrite(SXMotor[1], HIGH);
}

// turn right
void turn_right()
{
   digitalWrite(DXMotor[0], LOW);
   digitalWrite(SXMotor[1], LOW);

   digitalWrite(DXMotor[1], HIGH);
   digitalWrite(SXMotor[0], HIGH);
}

// have a break, have a rolano
void stop()
{
   for (byte x=0; x<2; x++)
   {
      digitalWrite(SXMotor[x], LOW);
      digitalWrite(DXMotor[x], LOW);
      digitalWrite(MEnable[x], LOW);
   }
}


// read a specific sensor to get its value
int readIR(byte pin, bool readSens)
{
   byte x;
   int obstacleIR, dist;

   if (readSens)
   {
      obstacleIR = analogRead(IRpins[pin]);   // storing IR coming from the obstacle
      dist = ambientIR[pin] - obstacleIR;     // calculating changes in IR values and storing it for future average
      digitalWrite(IRemitters[pin], LOW);     // turning the IR LED off
      return(dist);
   }

   else
   {
      ambientIR[pin] = analogRead(IRpins[pin]);  // storing IR coming from the ambient
      digitalWrite(IRemitters[pin], HIGH);       // turning the IR LED on to read the IR coming from the obstacle
      return(0);
   }
}

// Bluetooth functions //

// read bluetooth 
void bluetooth_control(bool forceUpdate)
{
   if (Serial1.available() > 0 || forceUpdate)
   {
      // empty bluetooth queue
      while (Serial1.available() > 0)
         btData = Serial1.read();

      //Serial.println(btData);

      // check this only if we accept manual control
      if (controlMode[0] == OPMODE)
      {

         if (btData == '0')
         {
            move_state(S);
         }

         else if (btData == '1')
         {
            move_state(F);
         }

         else if (btData == '2')
         {
            move_state(B);
         }

         else if (btData == '3')
         {
            move_state(L);
         }

         else if (btData == '4')
         { 
            move_state(R);
         }
      }

      // only toggle control modes when not in emergency state
      if (controlMode[0] >= OPMODE)
      {
         if (btData == '5')
         {
            heading = -1;
            controlMode[0] = OPMODE;
            move_state(S);
         }

         else if (btData == '6')
         {
            heading = -1;
            AIState = 0;
            AIDelay = 0;
            controlMode[0] = OPMODE + 1;
         }
      }
   }
}

// AI functions //

// we're about to fall
void fall_emergency(byte lCount)
{
   //Serial.println("EMERGENCY");

   AIState = 0;

   // storing current (non emergency) control mode
   if (controlMode[0] >= OPMODE)
      controlMode[1] = controlMode[0];

   /*for (byte x=0; x<IRS; x++)
     {
     Serial.println(fallAlarm[x]);
     }*/

   // all sensors triggered an alarm, maybe the robot was lifted
   if (fallAlarm[0] && fallAlarm[1] && fallAlarm[2] && fallAlarm[3])
   {
      controlMode[0] = 0;
      //Serial.println("ALL");
      move_state(S);
   }

   // front sensors triggered an alarm, we're probably about to suicide
   else if (fallAlarm[1] || fallAlarm[2])
   {
      controlMode[0] = 0;
      move_state(B);
      AIDelay = lCount + EMGDELAY;

      // we need to trigger fall sensors before the AI (20 * 4 milliseconds on default)
      FallSensorsDelay = AIDelay - (LIRDELAY + IRS) * IRS;

      // testing stuff
      if (controlMode[1] == OPMODE + 1)
      {
         if (fallAlarm[1])
            AIState = 1;

         else
            AIState = 3;
      }

      //Serial.println("FRONT");

      /*if (fallAlarm[0])
        Serial.println("AND LEFT");

        else if (fallAlarm[3])
        Serial.println("AND RIGHT");*/
   }

   // only left sensor triggered an alarm
   else if (fallAlarm[0])
   { 
      controlMode[0] = 1;
      AIDelay = lCount;
      //Serial.println("LEFT");

      /*if (fallAlarm[3])
        {
        controlMode[0] = 0;
        move_state(R);
        AIDelay = EMGDELAY;
        }*/
   }

   // only right sensor triggered an alarm
   else if (fallAlarm[3])
   {
      controlMode[0] = 2;
      AIDelay = lCount;
      //Serial.println("RIGHT");
   }

   // unknown, moonwalking can't be wrong 
   else
   {
      //Serial.println("UNKNOWN");
      controlMode[0] = 0;
      move_state(B);
   }
}

// testing ai step
void ai_step(int lCount)
{
   byte pMove;
   //Serial.println("AISTEP");

   heading = get_heading(); 

   // we were into an emergency
   if (controlMode[0] == 0)
   {
      // reset previous control mode
      controlMode[0] = controlMode[1];

      // the robot was on manual control mode 
      if (controlMode[0] == OPMODE)
      {
         // the robot was moving back, stop
         if (move == B)
            move_state(S);
      }
   }

   pMove = move;

   // stair left/right emergency 
   if (controlMode[0] == 1 || controlMode[0] == 2)
   {
      /*Serial.print("Control mode: ");
        Serial.println(controlMode[0]);

        Serial.print("AIState: ");
        Serial.println(AIState);

        Serial.print("Move: ");
        Serial.println(move);

        Serial.print("AIDelay: ");
        Serial.println(AIDelay);*/

      // we're done, reset stuff
      if (AIState > 3)
      {
         move_state(S);
         AIState = 0;
         controlMode[0] = controlMode[1];
         bluetooth_control(true); // reset previous manual move state
      }

      // lateral stair escape loop
      else
      {
         AIDelay = stairDelay[AIState];

         if (AIState == 0)
         {
            FallSensorsDelay = stairDelay[AIState];
         }

         //Serial.println(AIState);
         //Serial.println(AIDelay);

         if (controlMode[0] == 1)
         {
            move_state(stairLeft[AIState]);
         }

         else
         {
            move_state(stairRight[AIState]);
         }


         AIState++;
      }
   }

   // patrol mode loop
   else if (controlMode[0] == OPMODE + 1)
   {
      // we should turn left or right
      if (patrol[AIState] == L ||
            patrol[AIState] == R)
      {
         AIDelay += AITDELAY;

         if (tHeading == -1)
            set_target_heading(patrol[AIState], pDelay[AIState]);


         else
         {
            //Serial.println("TURN");
            //Serial.println(patrol[AIState]);

            heading = get_heading();

            move_state(patrol[AIState]);

            if ((move == L && sHeading > tHeading && heading <= tHeading) ||
                (move == R && sHeading < tHeading && heading >= tHeading))
            {
               //Serial.println("STOP");
               tHeading = -1;
               AIState++;
               AIDelay = lCount + 1;
            }

            // improve this part
            else if ((move == L && heading > sHeading) ||
                  (move == R && heading < sHeading))
            {
               sHeading = heading;
            }
         }
      }         

      else
      {
         AIDelay = pDelay[AIState];
         move_state(patrol[AIState]);
         AIState++;
      }

      if (AIState > 7)
         AIState = 0;
   }
}


// read our compass and set heading
float get_heading()
{
   Vector norm, acc;
   float fHead, roll, pitch, sinPitch;
   float cosRoll, sinRoll, cosPitch;
   float Xh, Yh;

   norm = compass.readNormalize();
   acc = accelerometer.readScaled();

   roll = asin(acc.YAxis);
   pitch = asin(-acc.XAxis);

   /*Serial.print("Roll: ");
   Serial.print(roll);
   Serial.print(" Pitch: ");
   Serial.println(pitch);*/

   // we can't tilt compensate use raw input
   if (roll > 0.78 || roll < -0.78 || pitch > 0.78 || pitch < -0.78)
   {
      //Serial.println("Non compensated");
      fHead = atan2(norm.YAxis, norm.XAxis);
   }

   // tilt compensation
   else
   {
      //Serial.println("Compensated");
      cosRoll = cos(roll);
      sinRoll = sin(roll);  
      cosPitch = cos(pitch);
      sinPitch = sin(pitch);

      Xh = norm.XAxis * cosPitch + norm.ZAxis * sinPitch;
      Yh = norm.XAxis * sinRoll * sinPitch + norm.YAxis * cosRoll - norm.ZAxis * sinRoll * cosPitch;

      fHead = atan2(Yh, Xh);
   }

   fHead += declinationAngle;

   // Correct for heading < 0deg and heading > 360deg
   if (fHead < 0)
      fHead += 2 * PI;

   if (fHead > 2 * PI)
      fHead -= 2 * PI;

   fHead *= 180/PI;   

   //Serial.println(fHead);

   return fHead; 
}


// set compass target heading
void set_target_heading(byte direction, int degr)
{
   bool left = direction == L;

   heading = get_heading(); 

   Serial.print("Setting initial heading to: ");
   Serial.println(heading);

   sHeading = heading;

   if (left)
   {
      Serial.print("LEFT");
      tHeading = heading - degr;
   }

   else
   {
      Serial.print("RIGHT");
      tHeading = heading + degr;
   }

   if (tHeading < 0)
      tHeading += 360;

   if (tHeading > 360)
      tHeading -= 360;


   Serial.print("Target heading: ");
   Serial.println(tHeading);
}
